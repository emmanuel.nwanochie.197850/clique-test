import React from 'react';

const UpIcon = () => {
    return (
        <svg
            width='17'
            height='16'
            viewBox='0 0 17 16'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
        >
            <path
                d='M12.3729 6.66934L8.46914 2.76562L4.56543 6.66934'
                stroke='white'
                stroke-width='1.92935'
                stroke-miterlimit='10'
                stroke-linecap='round'
                stroke-linejoin='round'
            />
            <path
                d='M8.46924 13.7006V2.87695'
                stroke='white'
                stroke-width='1.92935'
                stroke-miterlimit='10'
                stroke-linecap='round'
                stroke-linejoin='round'
            />
        </svg>
    );
};

export default UpIcon;
