import React from 'react';

const FallIcon = () => {
    return (
        <svg
            width='16'
            height='16'
            viewBox='0 0 16 16'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
        >
            <path
                d='M4.35175 9.79551L8.25547 13.6992L12.1592 9.79551'
                stroke='white'
                stroke-width='1.92935'
                stroke-miterlimit='10'
                stroke-linecap='round'
                stroke-linejoin='round'
            />
            <path
                d='M8.25537 2.76425L8.25537 13.5879'
                stroke='white'
                stroke-width='1.92935'
                stroke-miterlimit='10'
                stroke-linecap='round'
                stroke-linejoin='round'
            />
        </svg>
    );
};

export default FallIcon;
