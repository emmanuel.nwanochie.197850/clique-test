import { Icon } from "@chakra-ui/react";
import React from "react";

const CliqueGiftIcon = (props: any) => (
  <Icon viewBox="0 0 33 27" {...props}>
    <svg
      width="33"
      height="27"
      viewBox="0 0 33 27"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M27.1458 7.5504V15.0609C27.1458 19.5525 24.5791 21.4775 20.7291 21.4775H7.91037C7.25412 21.4775 6.62704 21.4192 6.04371 21.288C5.67912 21.2296 5.32914 21.1275 5.0083 21.0109C2.8208 20.1942 1.49371 18.2984 1.49371 15.0609V7.5504C1.49371 3.05874 4.06037 1.13379 7.91037 1.13379H20.7291C23.9958 1.13379 26.3437 2.5192 26.9708 5.68378C27.0729 6.26712 27.1458 6.86499 27.1458 7.5504Z"
        stroke="currentColor"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M31.5224 11.9252V19.4357C31.5224 23.9273 28.9557 25.8523 25.1057 25.8523H12.287C11.2078 25.8523 10.2307 25.7065 9.38488 25.3857C7.64947 24.744 6.4682 23.4169 6.04529 21.2878C6.62862 21.419 7.2557 21.4773 7.91195 21.4773H20.7307C24.5807 21.4773 27.1474 19.5523 27.1474 15.0607V7.55021C27.1474 6.8648 27.089 6.25234 26.9724 5.68359C29.7432 6.26693 31.5224 8.22105 31.5224 11.9252Z"
        stroke="currentColor"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.3102 15.1629C16.4365 15.1629 18.1602 13.4392 18.1602 11.3129C18.1602 9.18662 16.4365 7.46289 14.3102 7.46289C12.1839 7.46289 10.4602 9.18662 10.4602 11.3129C10.4602 13.4392 12.1839 15.1629 14.3102 15.1629Z"
        stroke="currentColor"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.97083 8.10449V14.5212"
        stroke="currentColor"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M22.6566 8.10547V14.5222"
        stroke="currentColor"
        strokeMiterlimit="10"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  </Icon>
);

export default CliqueGiftIcon;
