import React from 'react';

const PlusIcon = () => {
  return (
    <svg
      width='28'
      height='27'
      viewBox='0 0 28 27'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M7.58337 13.584H20.875'
        stroke='white'
        stroke-width='1.99375'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
      <path
        d='M14.2291 20.2292V6.9375'
        stroke='white'
        stroke-width='1.99375'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
    </svg>
  );
};

export default PlusIcon;
