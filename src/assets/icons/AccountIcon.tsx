import { Icon } from "@chakra-ui/react";
import React from "react";

const AccountIcon = () => (
  <Icon viewBox="0 0 30 30" width={"1.3em"} height={"1.3em"}>
    <svg
      width="30"
      height="30"
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.4"
        d="M15.1504 15.9746C15.0629 15.9621 14.9504 15.9621 14.8504 15.9746C12.6504 15.8996 10.9004 14.0996 10.9004 11.8871C10.9004 9.62461 12.7254 7.78711 15.0004 7.78711C17.2629 7.78711 19.1004 9.62461 19.1004 11.8871C19.0879 14.0996 17.3504 15.8996 15.1504 15.9746Z"
        stroke="currentColor"
        strokeOpacity="0.4"
        strokeWidthstroke-width="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.34"
        d="M23.4252 24.2242C21.2002 26.2617 18.2502 27.4992 15.0002 27.4992C11.7502 27.4992 8.8002 26.2617 6.5752 24.2242C6.7002 23.0492 7.4502 21.8992 8.7877 20.9992C12.2127 18.7242 17.8127 18.7242 21.2127 20.9992C22.5502 21.8992 23.3002 23.0492 23.4252 24.2242Z"
        stroke="currentColor"
        strokeOpacity="0.4"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15 27.5C21.9036 27.5 27.5 21.9036 27.5 15C27.5 8.09644 21.9036 2.5 15 2.5C8.09644 2.5 2.5 8.09644 2.5 15C2.5 21.9036 8.09644 27.5 15 27.5Z"
        stroke="currentColor"
        strokeOpacity="0.4"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  </Icon>
);

export default AccountIcon;


