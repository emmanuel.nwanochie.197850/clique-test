export interface IHomeLayoutProps {
  children?: React.ReactNode;
  upload?: () => void;
  toggleView?: boolean;
}
