import Common from './Common';

type Props = {};

const Faq = (props: Props) => {
  return (
    <Common
      title="Help and FAQs"
      subTitle="To reach out to the Circo Team concerning issues, send a mail to"
      firstHighlight="support@circo.com"
      main="Check our Frequently Asked Questions"
      highlight="FAQs"
    />
  );
};

export default Faq;
