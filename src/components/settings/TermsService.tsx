import Common from './Common';

type Props = {};

function TermsService({}: Props) {
  return (
    <Common
      title='Terms of Service'
      subTitle='Circos tersm of service has been set to keep it’s users saftey.'
      main='Review Circos'
      highlight='Terms of Service'
    />
  );
}

export default TermsService;
