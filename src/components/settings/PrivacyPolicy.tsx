import Common from './Common';

type Props = {};

function PrivacyPolicy({}: Props) {
  return (
    <Common
      title="Privacy Policy"
      subTitle="Circos privacy guideliness has been set to keep it’s users saftey."
      main="Review Circos"
      highlight="Privacy Policy"
    />
  );
}

export default PrivacyPolicy;
